#ifndef FILE_WORK_H
#define FILE_WORK_H


#include "bmp_work.h"


enum read_status file_open(const char* file_name, struct image *image);

enum write_status file_write(const char* file_name, struct image const *image);

#endif
