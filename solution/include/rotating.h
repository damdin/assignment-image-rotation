#ifndef ROTATING_H
#define ROTATING_H


#include "image.h"


struct image image_rotate(struct image const *image);

#endif
