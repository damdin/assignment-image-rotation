#ifndef ERRORS_H
#define ERRORS_H


#include "bmp_work.h"


const char* read_error_message(enum read_status status);

const char* write_error_message(enum write_status status);

#endif
