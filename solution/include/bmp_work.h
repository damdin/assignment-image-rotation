#ifndef BMP_WORK_H
#define BMP_WORK_H


#include "image.h"
#include <stdio.h>


enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NO_DATA,
    READ_CLOSING_ERROR,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status check_bmp_status(FILE *in_file, struct image *image);

enum write_status check_write_status(FILE *out_file, struct image const *image);

#endif
