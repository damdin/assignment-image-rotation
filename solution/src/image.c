#include "image.h"
#include <stdlib.h>


struct image image_create(uint32_t width, uint32_t height) {
    return (struct image) {.width = width, .height = height, .data = malloc(sizeof(struct pixel) * width * height)};
}

void image_free(struct image *image) {
    free(image -> data);
}
