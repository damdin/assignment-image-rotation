#include "errors.h"
#include "file_work.h"
#include "rotating.h"


int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "Incorrect number of arguments\n");
    }
    struct image image = {0};
    enum read_status input = file_open(argv[1], &image);
    if (input != READ_OK) {
        fprintf(stderr, "%s\n", read_error_message(input));
        fprintf(stdout, "Rotation failed\n");
        return input;
    }
    struct image image_result;
    image_result = image_rotate(&image);
    enum write_status output = file_write(argv[2], &image_result);
    if (output == WRITE_OK) {
        fprintf(stderr, "%s\n", write_error_message(output));
        fprintf(stdout, "Rotation failed\n");
    }
    image_free(&image);
    image_free(&image_result);
    return 0;
}
