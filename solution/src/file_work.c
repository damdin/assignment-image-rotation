#include "file_work.h"


enum read_status file_open(const char* file_name, struct image *image) {
    FILE *input_file;
    input_file = fopen(file_name, "rb");
    if (input_file == NULL) {
        fclose(input_file);
        return READ_NO_DATA;
    }
    enum read_status read_status;
    read_status = check_bmp_status(input_file, image);
    if (fclose(input_file)) {
        return READ_CLOSING_ERROR;
    }
    return read_status;
}

enum write_status file_write(const char* file_name, struct image const *image) {
    FILE *output_file;
    output_file = fopen(file_name, "wb");
    enum write_status write_status;
    write_status = check_write_status(output_file, image);
    if (fclose(output_file)) {
        return WRITE_ERROR;
    }
    return write_status;
}
