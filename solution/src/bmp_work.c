#include "bmp_work.h"


static uint8_t padding(struct image const *image) {
    return image -> width % 4;
}

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static struct bmp_header create_header(struct image const* image) {
    struct bmp_header new_header = {
            .bfType = 0X4D42,
            .bfileSize = sizeof(struct bmp_header) + padding(image) *  image -> height,
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image -> height * (image -> width + padding(image)),
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return new_header;
}

enum read_status check_bmp_status(FILE *in_file, struct image *image) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in_file) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != 0X4D42 || header.biBitCount != 24 || header.bfReserved != 0 || header.biSize != 40) {
        return READ_INVALID_SIGNATURE;
    }
    uint32_t bits = 0;
    *image = image_create(header.biWidth, header.biHeight);
    const uint8_t bmp_padding = padding(image);
    for(size_t i = 0; i < image -> height; i++) {
        bits = fread(&(image -> data[image -> width * i]), sizeof(struct pixel), image -> width, in_file);
        if (bits != image -> width) {
            image_free(image);
            return READ_INVALID_BITS;
        }
        if (fseek(in_file, bmp_padding, SEEK_CUR) != 0) {
            image_free(image);
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status check_write_status(FILE *out_file, struct image const *image) {
    struct bmp_header header = create_header(image);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out_file) != 1) {
        return WRITE_ERROR;
    }
    const uint8_t bmp_padding = padding(image);
    const uint32_t waste = 0;
    for(size_t i = 0; i < image -> height; i++) {
        if (!fwrite(&image -> data[i * image -> width], sizeof(struct pixel), image -> width, out_file)) {
            return WRITE_ERROR;
        }
        if (!fwrite(&waste, 1, bmp_padding, out_file)) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
