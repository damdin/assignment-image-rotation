#include "rotating.h"
#include <stdlib.h>


struct image image_rotate(struct image const *image) {
    struct image image_result;
    image_result.height = image -> width;
    image_result.width = image -> height;
    image_result.data = malloc(sizeof(struct pixel) * image -> height * image -> width);
    for (size_t i = 0; i < image -> height; i++) {
        for (size_t j = 0; j < image -> width; j++) {
            image_result.data[(image -> height - 1 - i) + j  * image -> height] = image -> data[i * image -> width + j];
        }
    }
    return image_result;
}
