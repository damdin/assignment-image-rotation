#include "errors.h"


const char* read_error_messages[] = {
        [READ_INVALID_SIGNATURE] = "Invalid type of file",
        [READ_INVALID_BITS] = "Number of bits is incorrect",
        [READ_INVALID_HEADER] = "Invalid BMP header",
        [READ_NO_DATA] = "No data found",
        [READ_CLOSING_ERROR] = "File wasn't closed",
        [READ_ERROR] = "An error with reading this file"
};

const char* write_error_messages[] = {
        [WRITE_ERROR] = "An error with writing this file"
};

const char* read_error_message(enum read_status status) {
    return read_error_messages[status];
}

const char* write_error_message(enum write_status status) {
    return write_error_messages[status];
}
